### Processo Seletivo Cabtec ###

Teste para desenvolvedor Java (foco em JSF)

Desenvolver um aplicativo para cadastro de agenda de contatos e anotação de recados.

O sistema será utilizado pela recepcionista da empresa, que lançará os recados, e pelos demais funcionários para consultar os seus respectivos recados, podendo lançar recados para outros contatos também.

### O que vale destacar no código implementado? ###

 O Código foi implementado conforme as seguintes tecnologias:

* Java EE 7 (EJB 3.2, JPA 2.1, JSF 2.2);
* Wildfly 8;
* Primefaces 6;
* Testes unitários(Arquillian);
* Maven;
* CDI 1.1;
* Lombok.

### O que poderia ser feito para melhorar o sistema? ###

* Poderia ter feito teste de aceitação com o Selenium.
* Poderia ter feito implentação de login com o JAAS ou Spring Security.

### Configurações ###


* Configuração datasource no Wildfly:

```
<datasource jta="false" jndi-name="java:jboss/datasources/ds_prova_ds" pool-name="ds_prova_ds" enabled="true" use-ccm="false">
                    <connection-url>jdbc:mysql://localhost:3306/ds_prova_ds</connection-url>
                    <driver-class>com.mysql.jdbc.Driver</driver-class>
                    <driver>mysql</driver>
                    <security>
                        <user-name>root</user-name>
                        <password>daluz</password>
                    </security>
                    <validation>
                        <validate-on-match>false</validate-on-match>
                        <background-validation>false</background-validation>
                    </validation>
                    <statement>
                        <share-prepared-statements>false</share-prepared-statements>
                    </statement>
</datasource>
```

