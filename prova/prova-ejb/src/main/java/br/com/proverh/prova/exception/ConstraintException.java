package br.com.proverh.prova.exception;

public class ConstraintException extends Exception {

	private static final long serialVersionUID = 1L;

	public ConstraintException(String mensagem) {
		super(mensagem);
	}
}
