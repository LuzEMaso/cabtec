package br.com.proverh.prova.exception;

public class DAOException extends Exception {
	private static final long serialVersionUID = 1L;

	public DAOException(String mensagem) {
		super(mensagem);
	}
}
