package br.com.proverh.prova.exception;

public class RegraDeNegocioException extends Exception {
	private static final long serialVersionUID = 1L;

	public RegraDeNegocioException(String mensagem) {
		super(mensagem);
	}
}
