package br.com.proverh.prova.model.dao;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;

import br.com.proverh.prova.exception.DAOException;
import br.com.proverh.prova.model.entity.Contato;
import br.com.proverh.prova.model.enuns.Mensagem;
import br.com.proverh.prova.model.to.LoginTO;
import br.com.proverh.prova.param.ContatoParam;

public class ContatoDAO extends HibernateDAO<Contato, Long> {

	private static final long serialVersionUID = 1L;

	public Contato logar(LoginTO loginTO) throws DAOException {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT c ").append(" FROM Contato c ").append(" WHERE c.login = :login ")
					.append(" AND c.senha = :senha");
			Query query = getEntityManager().createQuery(sql.toString());
			query.setParameter("login", loginTO.getLogin());
			query.setParameter("senha", loginTO.getSenha());
			if (!query.getResultList().isEmpty())
				return (Contato) query.getResultList().get(0);
			else {
				throw new DAOException(Mensagem.ERRO_LOGIN.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException(Mensagem.ERRO_LOGIN.getValue());
		}
	}

	public Boolean verificarSeLoginExiste(LoginTO loginTO) throws DAOException {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT c ").append(" FROM Contato c ").append(" WHERE c.login = :login ");
			Query query = getEntityManager().createQuery(sql.toString());
			query.setParameter("login", loginTO.getLogin());
			return query.getResultList().isEmpty();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException(Mensagem.ERRO_BUSCAR_CONTATO.getValue());
		}
	}

	public List<Contato> pesquisar(ContatoParam contatoParam) throws DAOException {
		StringBuilder sql = new StringBuilder();
		try {
			sql.append(" SELECT c ").append(" FROM Contato c ").append(" WHERE 1 = 1 ");

			if (!StringUtils.isBlank(contatoParam.getLogin())) {
				sql.append(" AND c.login = :login");
			}
			if (!StringUtils.isBlank(contatoParam.getNome())) {
				sql.append(" AND c.nome = :nome");
			}
			if (contatoParam.getRamal() != null) {
				sql.append(" AND c.ramal = :ramal");
			}
			if (!StringUtils.isBlank(contatoParam.getEmail())) {
				sql.append(" AND c.email = :email");
			}

			TypedQuery<Contato> query = getEntityManager().createQuery(sql.toString(), Contato.class);

			if (!StringUtils.isBlank(contatoParam.getLogin())) {
				query.setParameter("login", contatoParam.getLogin());
			}
			if (!StringUtils.isBlank(contatoParam.getNome())) {
				query.setParameter("nome", contatoParam.getNome());
			}
			if (contatoParam.getRamal() != null) {
				query.setParameter("ramal", contatoParam.getRamal());
			}
			if (!StringUtils.isBlank(contatoParam.getEmail())) {
				query.setParameter("email", contatoParam.getEmail());
			}
			return query.getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException(Mensagem.ERRO_BUSCA_DINAMICA.getValue());
		}
	}

	public List<Contato> listarCincoRecadosMaiAntigos() throws DAOException {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT r.idContato ").append(" FROM Recado r ").append(" ORDER BY r.horario DESC");
			TypedQuery<Contato> query = getEntityManager().createQuery(sql.toString(), Contato.class);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException(Mensagem.ERRO_BUSCAR_CONTATO.getValue());
		}
	}

}
