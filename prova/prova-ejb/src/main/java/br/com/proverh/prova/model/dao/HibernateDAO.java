package br.com.proverh.prova.model.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import br.com.proverh.prova.exception.ConstraintException;
import br.com.proverh.prova.exception.DAOException;
import br.com.proverh.prova.model.enuns.Mensagem;
import lombok.Getter;

@Stateless
@SuppressWarnings({ "unchecked" })
public class HibernateDAO<T extends Serializable, PK extends Serializable> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	@Getter
	private EntityManager entityManager;

	public HibernateDAO() {

	}

	public void persist(T objeto) throws DAOException, ConstraintException {
		try {
			entityManager.persist(objeto);
			entityManager.flush();
		} catch (ConstraintViolationException c) {
			Set<ConstraintViolation<?>> cvs = c.getConstraintViolations();
			String errMsg = "";
			for (ConstraintViolation<?> cv : cvs) {
				errMsg = cv.getMessage();
			}
			throw new ConstraintViolationException(errMsg, cvs);
		} catch (PersistenceException e) {
			if (e.getMessage().contains("ConstraintViolationException")) {
				throw new ConstraintException(e.getMessage());
			} else {
				throw e;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException(Mensagem.ERRO_SALVA.getValue());
		}
	}

	public T merge(T objeto) throws DAOException, ConstraintException {
		T entidade = null;
		try {
			entidade = (T) entityManager.merge(objeto);
			entityManager.flush();
		} catch (ConstraintViolationException c) {
			Set<ConstraintViolation<?>> cvs = c.getConstraintViolations();
			String errMsg = "";
			for (ConstraintViolation<?> cv : cvs) {
				errMsg = cv.getMessage();
			}
			throw new DAOException(errMsg);
		} catch (PersistenceException e) {
			if (e.getMessage().contains("ConstraintViolationException")) {
				throw new ConstraintException(e.getMessage());
			} else {
				throw e;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException(Mensagem.ERRO_ATUALIZA.getValue());
		}
		return entidade;
	}

	public void remove(T objeto) throws DAOException {
		try {
			entityManager.remove(entityManager.contains(objeto) ? objeto : entityManager.merge(objeto));
			entityManager.flush();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException(Mensagem.ERRO_DELETA.getValue());
		}
	}

	public T findById(PK pk) throws DAOException {
		try {
			return (T) entityManager.find(getTypeClass(), pk);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException(Mensagem.ERRO_BUSCA_POR_ID.getValue());
		}
	}

	public Collection<T> listAll() throws DAOException {
		try {
			return entityManager.createQuery("FROM " + getTypeClass().getName()).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException(Mensagem.ERRO_BUSCA_TUDO.getValue());
		}
	}

	private Class<T> getTypeClass() {
		return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

}