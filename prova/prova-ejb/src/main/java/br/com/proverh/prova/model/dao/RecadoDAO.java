package br.com.proverh.prova.model.dao;

import java.util.Collection;
import java.util.List;

import javax.persistence.TypedQuery;

import br.com.proverh.prova.exception.DAOException;
import br.com.proverh.prova.model.entity.Contato;
import br.com.proverh.prova.model.entity.Recado;

public class RecadoDAO extends HibernateDAO<Recado, Long> {

	private static final long serialVersionUID = 1L;

	public List<Recado> retornarRecadosExpirados() throws DAOException {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT r ").append(" FROM Recado r ").append(" WHERE DATEDIFF(hour,r.horario, NOW()) > 2");
			TypedQuery<Recado> query = getEntityManager().createQuery(sql.toString(), Recado.class);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException(e.getMessage());
		}
	}

	public Collection<Recado> visualizar(Contato contato) throws DAOException {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT r ").append(" FROM Recado r ")
					.append(" WHERE r.idContato =:contato or r.idContato.login = r.gtiCreatedBy");
			TypedQuery<Recado> query = getEntityManager().createQuery(sql.toString(), Recado.class);
			query.setParameter("contato", contato);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException(e.getMessage());
		}
	}

	public Collection<Recado> listarTodosRecadosContato(Long id) throws DAOException {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT r ").append(" FROM Recado r ").append(" WHERE r.idContato.id =:id");
			TypedQuery<Recado> query = getEntityManager().createQuery(sql.toString(), Recado.class);
			query.setParameter("id", id);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException(e.getMessage());
		}
	}

	public Collection<Recado> listarTodosDoDia(Long id) throws DAOException {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT r ").append(" FROM Recado r ")
					.append(" WHERE r.horario >= CURDATE() AND r.idContato.id =:id ");
			TypedQuery<Recado> query = getEntityManager().createQuery(sql.toString(), Recado.class);
			query.setParameter("id", id);
			return query.getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException(e.getMessage());
		}
	}

}
