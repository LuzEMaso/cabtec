package br.com.proverh.prova.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;

@Entity
@Table(name = "CONTATO", indexes = { @Index(columnList = "nome", name = "IDX_contatos_nome"),
		@Index(columnList = "ramal", name = "IDX_contatos_ramal") })
public @Data class Contato implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false, unique = true)
	private Long id;

	@NotNull(message = "Campo login não pode ser vazio!")
	@NotEmpty(message = "Campo login não pode ser vazio!")
	@Column(name = "LOGIN", nullable = false, length = 140)
	private String login;

	@NotEmpty(message = "Campo senha não pode ser vazio!")
	@NotNull(message = "Campo senha não pode ser vazio!")
	@Column(name = "SENHA", nullable = false, length = 255)
	private String senha;

	@NotEmpty
	@NotNull(message = "Campo nome não pode ser vazio!")
	@Column(name = "NOME", nullable = false, length = 140)
	private String nome;

	@NotNull(message = "Campo ramal não pode ser vazio!")
	@Column(name = "RAMAL", nullable = false)
	private Integer ramal;

	@Column(name = "EMAIL", nullable = true, length = 255)
	private String email;

	@Temporal(TemporalType.TIMESTAMP)
	@NotNull(message = "Campo gtiCreatedAt não pode ser vazio!")
	@Column(name = "GTI_CREATED_AT", nullable = false)
	private Date gtiCreatedAt;

	@NotEmpty(message = "Campo gtiCreatedBy não pode ser vazio!")
	@NotNull(message = "Campo gtiCreatedBy não pode ser vazio!")
	@Column(name = "GTI_CREATED_BY", nullable = false, length = 140)
	private String gtiCreatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@NotNull(message = "Campo gtiModifiedAt não pode ser vazio!")
	@Column(name = "GTI_MODIFIED_AT", nullable = false)
	private Date gtiModifiedAt;

	@NotEmpty(message = "Campo gtiModifiedByd não pode ser vazio!")
	@NotNull(message = "Campo gtiModifiedByd não pode ser vazio!")
	@Column(name = "GTI_MODIFIED_BY", nullable = false, length = 140)
	private String gtiModifiedByd;

	@NotNull(message = "Campo gtiVersion não pode ser vazio!")
	@Column(name = "GTI_VERSION", nullable = false)
	private Integer gtiVersion;

	@Override
	public String toString() {
		return nome;
	}

	public void auditoriaCreate() {
		gtiCreatedAt = new Date();
		gtiCreatedBy = login;
		gtiModifiedAt = new Date();
		gtiModifiedByd = login;
		gtiVersion = 1;
	}

}
