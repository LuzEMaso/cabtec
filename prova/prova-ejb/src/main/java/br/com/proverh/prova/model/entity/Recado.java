package br.com.proverh.prova.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.proverh.prova.exception.RegraDeNegocioException;
import br.com.proverh.prova.model.enuns.Mensagem;
import lombok.Data;

@Entity
@Table(name = "RECADO")
public @Data class Recado implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false, unique = true)
	private Long id;

	@OneToOne
	@NotNull(message = "Campo idContato não pode ser vazio!")
	@JoinColumn(name = "FK_RECADO_CONTATO", nullable = false)
	private Contato idContato;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "HORARIO", nullable = false)
	@NotNull(message = "Campo horario não pode ser vazio!")
	private Date horario;

	@NotNull(message = "Campo quem não pode ser vazio!")
	@Column(name = "QUEM", nullable = false, length = 140)
	private String quem;

	@NotNull(message = "Campo texto não pode ser vazio!")
	@Column(name = "TEXTO", nullable = false)
	private String texto;

	@NotNull(message = "Campo indLido não pode ser vazio!")
	@Column(name = "IND_LIDO", nullable = false, length = 1)
	private Integer indLido;

	@Temporal(TemporalType.TIMESTAMP)
	@NotNull(message = "Campo gtiCreatedAt não pode ser vazio!")
	@Column(name = "GTI_CREATED_AT", nullable = false)
	private Date gtiCreatedAt;

	@NotEmpty(message = "Campo gtiCreatedBy não pode ser vazio!")
	@NotNull(message = "Campo gtiCreatedBy não pode ser vazio!")
	@Column(name = "GTI_CREATED_BY", nullable = false, length = 140)
	private String gtiCreatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@NotNull(message = "Campo gtiModifiedAt não pode ser vazio!")
	@Column(name = "GTI_MODIFIED_AT", nullable = false)
	private Date gtiModifiedAt;

	@NotEmpty(message = "Campo gtiModifiedByd não pode ser vazio!")
	@NotNull(message = "Campo gtiModifiedByd não pode ser vazio!")
	@Column(name = "GTI_MODIFIED_BY", nullable = false, length = 140)
	private String gtiModifiedByd;

	@NotNull(message = "Campo gtiVersion não pode ser vazio!")
	@Column(name = "GTI_VERSION", nullable = false)
	private Integer gtiVersion;

	public void marcarComoLido() {
		indLido = 1;
	}

	public void recadoJaFoiLido() throws RegraDeNegocioException {
		if (indLido == 1) {
			throw new RegraDeNegocioException(Mensagem.RECADO_JA_LIDO.getValue());
		}
	}

	public void recadoNaoFoiLido() throws RegraDeNegocioException {
		if (indLido == 0) {
			throw new RegraDeNegocioException(Mensagem.RECADO_NAO_LIDO.getValue());
		}
	}

	public void marcarComoNaoLido() {
		indLido = 0;
	}

	public void auditoriaCreate() {
		indLido = 0;
		gtiCreatedAt = new Date();
		gtiCreatedBy = quem;
		gtiModifiedAt = new Date();
		gtiModifiedByd = quem;
		horario = new Date();
		gtiVersion = 1;
	}

	public void auditoriaModified() {
		gtiModifiedAt = new Date();
		gtiModifiedByd = quem;
	}

}
