package br.com.proverh.prova.model.enuns;

import lombok.Getter;
import lombok.Setter;

public enum Mensagem {
						LOGIN_VAZIO("Campo login não pode ser vazio!"),
						ERRO_SALVA("Erro ao salvar!"),
						ERRO_ATUALIZA("Erro ao atualizar!"),
						ERRO_DELETA("Erro ao deletar!"),
						ERRO_BUSCA_POR_ID("Erro ao buscar por id!"),
						ERRO_BUSCA_TUDO("Erro ao buscar tudo!"),
						CADASTRO_JA_FEITO("Cadastro já feito!"),
						CONTATO_VAZIO("Campo idContato não pode ser vazio!"),
						SENHA_VAZIO("Campo senha não pode ser vazio!"),
						ERRO_LOGIN("Usuário ou senha inválido!"),
						ERRO_LOGIN_NAO_CADASTRADO("Login não cadastrado!"),
						CONTATO_NAO_CADASTRADO("Contato não foi cadastrado!"),
						ERRO_BUSCAR_CONTATO("Erro ao fazer busca de contato!"),
						ERRO_BUSCA_DINAMICA("Erro ao fazer busca dinâmica!"),
						RECADO_JA_REGISTRADO("Recado já foi registrado!"),
						RECADO_VAZIO("Campo recado não pode ser vazio!"),
						RECADO_JA_LIDO("Recado já foi lido!"),
						RECADO_NAO_LIDO("Recado não foi lido!");

	@Getter
	@Setter
	private String value;

	private Mensagem(String value) {
		this.value = value;
	}

}
