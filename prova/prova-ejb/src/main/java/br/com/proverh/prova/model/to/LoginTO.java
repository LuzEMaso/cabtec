package br.com.proverh.prova.model.to;

import java.io.Serializable;

import br.com.proverh.prova.exception.RegraDeNegocioException;
import br.com.proverh.prova.model.enuns.Mensagem;
import lombok.Data;

public @Data class LoginTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String login;
	private String senha;

	public void loginEstaVazio() throws RegraDeNegocioException {
		if (login == null || login.isEmpty()) {
			throw new RegraDeNegocioException(Mensagem.LOGIN_VAZIO.getValue());
		}
	}

	public void senhaEstaVazio() throws RegraDeNegocioException {
		if (senha == null || senha.isEmpty()) {
			throw new RegraDeNegocioException(Mensagem.SENHA_VAZIO.getValue());
		}
	}

}
