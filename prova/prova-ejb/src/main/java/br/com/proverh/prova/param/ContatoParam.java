package br.com.proverh.prova.param;

import java.io.Serializable;

import lombok.Data;

public @Data class ContatoParam implements Serializable {

	private static final long serialVersionUID = 1L;

	private String login;
	private String nome;
	private Integer ramal;
	private String email;

}
