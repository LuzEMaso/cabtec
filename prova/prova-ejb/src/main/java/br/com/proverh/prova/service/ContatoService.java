package br.com.proverh.prova.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.proverh.prova.exception.ConstraintException;
import br.com.proverh.prova.exception.DAOException;
import br.com.proverh.prova.exception.RegraDeNegocioException;
import br.com.proverh.prova.model.dao.ContatoDAO;
import br.com.proverh.prova.model.entity.Contato;
import br.com.proverh.prova.model.enuns.Mensagem;
import br.com.proverh.prova.model.to.LoginTO;
import br.com.proverh.prova.param.ContatoParam;
import br.com.proverh.prova.util.SenhaUtil;

@Stateless
public class ContatoService {

	@Inject
	private ContatoDAO contatoDAO;

	public void cadastrar(Contato contato) throws DAOException {
		try {
			contato.auditoriaCreate();
			contato.setSenha(new SenhaUtil().criptografarSenha(contato.getSenha()));
			contatoDAO.persist(contato);
		} catch (ConstraintException e) {
			throw new DAOException(Mensagem.CADASTRO_JA_FEITO.getValue());
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException(e.getMessage());
		}
	}

	public Contato findById(Long id) throws DAOException {
		return contatoDAO.findById(id);
	}

	public Contato logar(LoginTO loginTO) throws RegraDeNegocioException {
		loginTO.loginEstaVazio();
		loginTO.senhaEstaVazio();
		try {
			verificarSeLoginExiste(loginTO);
			loginTO.setSenha(new SenhaUtil().criptografarSenha(loginTO.getSenha()));
			return contatoDAO.logar(loginTO);
		} catch (Exception e) {
			throw new RegraDeNegocioException(e.getMessage());
		}

	}

	private void verificarSeLoginExiste(LoginTO loginTO) throws DAOException, RegraDeNegocioException {
		if (contatoDAO.verificarSeLoginExiste(loginTO)) {
			throw new RegraDeNegocioException(Mensagem.CONTATO_NAO_CADASTRADO.getValue());
		}
	}

	public List<Contato> pesquisar(ContatoParam contatoParam) throws DAOException {
		return contatoDAO.pesquisar(contatoParam);
	}

	public List<Contato> listarCincoRecadosMaiAntigos() throws DAOException {
		return contatoDAO.listarCincoRecadosMaiAntigos();
	}
}
