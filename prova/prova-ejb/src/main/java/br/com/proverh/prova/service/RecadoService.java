package br.com.proverh.prova.service;

import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.proverh.prova.exception.ConstraintException;
import br.com.proverh.prova.exception.DAOException;
import br.com.proverh.prova.exception.RegraDeNegocioException;
import br.com.proverh.prova.model.dao.RecadoDAO;
import br.com.proverh.prova.model.entity.Contato;
import br.com.proverh.prova.model.entity.Recado;
import br.com.proverh.prova.model.enuns.Mensagem;

@Stateless
public class RecadoService {

	@Inject
	private Logger logger;

	@Inject
	private RecadoDAO recadoDAO;

	public void registrar(Recado recado) throws DAOException {
		try {
			recado.auditoriaCreate();
			recadoDAO.persist(recado);
		} catch (ConstraintException e) {
			throw new DAOException(Mensagem.RECADO_JA_REGISTRADO.getValue());
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException(e.getMessage());
		}
	}

	public Recado findById(Long id) throws DAOException {
		return recadoDAO.findById(id);
	}

	public void marcaComoLido(Recado recado) throws DAOException, RegraDeNegocioException {
		verificarSeRecadoEstaVazio(recado);
		recado.recadoJaFoiLido();
		recado.auditoriaModified();
		recado.marcarComoLido();
		try {
			recadoDAO.merge(recado);
		} catch (ConstraintException e) {
			throw new DAOException(Mensagem.RECADO_JA_REGISTRADO.getValue());
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException(e.getMessage());
		}

	}

	private void verificarSeRecadoEstaVazio(Recado recado) throws RegraDeNegocioException {
		if (recado == null) {
			throw new RegraDeNegocioException(Mensagem.RECADO_VAZIO.getValue());
		}
	}

	public void marcaComoNaoLido(Recado recado) throws RegraDeNegocioException, DAOException {
		verificarSeRecadoEstaVazio(recado);
		recado.recadoNaoFoiLido();
		recado.auditoriaModified();
		recado.marcarComoNaoLido();
		try {
			recadoDAO.merge(recado);
		} catch (ConstraintException e) {
			throw new DAOException(Mensagem.RECADO_JA_REGISTRADO.getValue());
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException(e.getMessage());
		}

	}

	@Schedule(dayOfWeek = "*", hour = "*/1")
	public void apagar() throws RegraDeNegocioException, DAOException {
		List<Recado> recados = recadoDAO.retornarRecadosExpirados();
		logger.info("Foram excluidos " + recados.size() + "recado(s)");
		for (Recado recado : recados) {
			recadoDAO.remove(recado);
		}
	}

	public Collection<Recado> listAll() throws DAOException {
		return recadoDAO.listAll();
	}

	public Collection<Recado> listarTodosDoDia(Long id) throws DAOException, RegraDeNegocioException {
		verificarSeIdContatoEstaVazio(id);
		return recadoDAO.listarTodosDoDia(id);
	}

	public Collection<Recado> visualizar(Contato contato) throws RegraDeNegocioException, DAOException {
		verificarSeContatoEstaVazio(contato);
		return recadoDAO.visualizar(contato);
	}

	private void verificarSeContatoEstaVazio(Contato contato) throws RegraDeNegocioException {
		if (contato == null) {
			throw new RegraDeNegocioException(Mensagem.CONTATO_VAZIO.getValue());
		}
	}

	public Collection<Recado> listarTodosRecadosContato(Long id) throws DAOException, RegraDeNegocioException {
		verificarSeIdContatoEstaVazio(id);
		return recadoDAO.listarTodosRecadosContato(id);
	}

	private void verificarSeIdContatoEstaVazio(Long id) throws RegraDeNegocioException {
		if (id == null || id == 0L) {
			throw new RegraDeNegocioException(Mensagem.CONTATO_VAZIO.getValue());
		}

	}

}
