package br.com.proverh.prova.asserts;

import org.junit.Assert;

import br.com.proverh.prova.model.entity.Contato;

public class ContatoAssert {

	private Contato contato;

	public ContatoAssert(Contato contato) {
		this.contato = contato;
	}

	public static ContatoAssert assertThat(Contato contato) {
		return new ContatoAssert(contato);
	}

	public ContatoAssert hasNome(String nome) {
		Assert.assertEquals(nome, contato.getNome());
		return this;
	}

}
