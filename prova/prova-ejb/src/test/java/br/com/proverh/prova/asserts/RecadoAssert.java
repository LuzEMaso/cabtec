package br.com.proverh.prova.asserts;

import org.junit.Assert;

import br.com.proverh.prova.model.entity.Recado;

public class RecadoAssert {

	private Recado recado;

	public static RecadoAssert assertThat(Recado recado) {
		return new RecadoAssert(recado);
	}

	public RecadoAssert(Recado recado) {
		this.recado = recado;
	}

	public RecadoAssert hasQuem(String quem) {
		Assert.assertEquals(quem, recado.getQuem());
		return this;
	}

	public RecadoAssert hasIndLido(Integer indLido) {
		Assert.assertEquals(indLido, recado.getIndLido());
		return this;
	}

}
