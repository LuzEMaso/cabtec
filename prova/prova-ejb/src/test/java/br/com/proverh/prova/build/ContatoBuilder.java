package br.com.proverh.prova.build;

import java.util.Date;

import br.com.proverh.prova.model.entity.Contato;

public class ContatoBuilder {

	private Contato contato = new Contato();

	public ContatoBuilder login(String login) {
		contato.setLogin(login);
		return this;
	}

	public ContatoBuilder email(String email) {
		contato.setEmail(email);
		return this;
	}

	public ContatoBuilder senha(String senha) {
		contato.setSenha(senha);
		return this;
	}

	public ContatoBuilder ramal(Integer ramal) {
		contato.setRamal(ramal);
		return this;
	}

	public ContatoBuilder gtiCreatedAt(Date gtiCreatedAt) {
		contato.setGtiCreatedAt(gtiCreatedAt);
		return this;
	}

	public ContatoBuilder gtiCreatedBy(String gtiCreatedBy) {
		contato.setGtiCreatedBy(gtiCreatedBy);
		return this;
	}

	public ContatoBuilder gtiModifiedAt(Date gtiModifiedAt) {
		contato.setGtiModifiedAt(gtiModifiedAt);
		return this;
	}

	public ContatoBuilder gtiModifiedBy(String gtiModifiedBy) {
		contato.setGtiModifiedByd(gtiModifiedBy);
		return this;
	}

	public ContatoBuilder gtiVersion(Integer gtiVersion) {
		contato.setGtiVersion(gtiVersion);
		return this;
	}

	public ContatoBuilder nome(String nome) {
		contato.setNome(nome);
		return this;
	}

	public Contato buld() {
		return contato;
	}

}
