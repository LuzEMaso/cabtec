package br.com.proverh.prova.build;

import br.com.proverh.prova.param.ContatoParam;

public class ContatoParamBuilder {

	private ContatoParam contatoParam = new ContatoParam();

	public ContatoParamBuilder login(String login) {
		contatoParam.setLogin(login);
		return this;
	}

	public ContatoParamBuilder nome(String nome) {
		contatoParam.setNome(nome);
		return this;
	}

	public ContatoParamBuilder ramal(Integer ramal) {
		contatoParam.setRamal(ramal);
		return this;
	}

	public ContatoParamBuilder email(String email) {
		contatoParam.setEmail(email);
		return this;
	}

	public ContatoParam build() {
		return contatoParam;
	}

}
