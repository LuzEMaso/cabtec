package br.com.proverh.prova.build;

import br.com.proverh.prova.model.to.LoginTO;

public class LoginTOBuilder {

	private LoginTO loginTO = new LoginTO();

	public LoginTOBuilder login(String login) {
		loginTO.setLogin(login);
		return this;
	}

	public LoginTOBuilder senha(String senha) {
		loginTO.setSenha(senha);
		return this;
	}

	public LoginTO build() {
		return loginTO;
	}

}
