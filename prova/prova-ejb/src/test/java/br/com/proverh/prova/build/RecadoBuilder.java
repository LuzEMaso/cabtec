package br.com.proverh.prova.build;

import java.util.Date;

import br.com.proverh.prova.model.entity.Contato;
import br.com.proverh.prova.model.entity.Recado;

public class RecadoBuilder {

	private Recado recado = new Recado();

	public RecadoBuilder idContato(Contato idContato) {
		// recado.setIdContato(idContato);
		return this;
	}

	public RecadoBuilder horario(Date horario) {
		recado.setHorario(horario);
		return this;
	}

	public RecadoBuilder quem(String quem) {
		recado.setQuem(quem);
		return this;
	}

	public RecadoBuilder texto(String texto) {
		recado.setTexto(texto);
		return this;
	}

	public RecadoBuilder indLido(Integer indLido) {
		recado.setIndLido(indLido);
		return this;
	}

	public RecadoBuilder gtiCreatedAt(Date gtiCreatedAt) {
		recado.setGtiCreatedAt(gtiCreatedAt);
		return this;
	}

	public RecadoBuilder gtiCreatedBy(String gtiCreatedBy) {
		recado.setGtiCreatedBy(gtiCreatedBy);
		return this;
	}

	public RecadoBuilder gtiModifiedAt(Date gtiModifiedAt) {
		recado.setGtiModifiedAt(gtiModifiedAt);
		return this;
	}

	public RecadoBuilder gtiModifiedByd(String gtiModifiedByd) {
		recado.setGtiModifiedByd(gtiModifiedByd);
		return this;
	}

	public RecadoBuilder gtiVersion(Integer gtiVersion) {
		recado.setGtiVersion(gtiVersion);
		return this;
	}

	public Recado buld() {
		return recado;
	}
}
