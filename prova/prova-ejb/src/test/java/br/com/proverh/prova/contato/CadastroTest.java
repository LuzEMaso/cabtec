package br.com.proverh.prova.contato;

import static org.junit.Assert.*;

import java.util.Date;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.proverh.prova.asserts.ContatoAssert;
import br.com.proverh.prova.build.ContatoBuilder;
import br.com.proverh.prova.model.entity.Contato;
import br.com.proverh.prova.model.enuns.Mensagem;
import br.com.proverh.prova.service.ContatoService;

@RunWith(Arquillian.class)
public class CadastroTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(WebArchive.class, "test.war").addPackages(true, "org.assertj.core")
				.addPackages(true, "org.apache.commons.lang3").addPackages(true, "br.com.proverh.prova")
				.addPackages(true, "io.jsonwebtoken").addPackages(true, "com.fasterxml.jackson")
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml").addAsResource("import.sql")
				.addAsWebInfResource("test-ds.xml", "test-ds.xml");
	}

	@Inject
	private ContatoService contatoService;

	@Test
	@InSequence(value = 1)
	public void whenLoginEstaVazio_shouldRetornarLoginNaoPodeSerVazio() {
		try {
			Contato contato = new ContatoBuilder().login(null).nome("Luiz Eduardo Silva Andrade")
					.email("luizcmaso@gmail.com").senha("luiz123").ramal(1234).gtiCreatedAt(new Date())
					.gtiCreatedBy("luizEdu").gtiModifiedAt(new Date()).gtiModifiedBy("luizEdu").gtiVersion(1).buld();

			contatoService.cadastrar(contato);
			fail("Expected an RegraDeNegocioException to be thrown");
		} catch (Exception e) {
			assertEquals(Mensagem.LOGIN_VAZIO.getValue(), e.getMessage());
		}
	}

	@Test
	@InSequence(value = 2)
	public void whenCompleto_shouldRetornarNomeContato() {
		try {
			Contato contato = new ContatoBuilder().login("luiz123").nome("Luiz Eduardo Silva Andrade")
					.email("luizcmaso@gmail.com").senha("luiz123").ramal(1234).gtiCreatedAt(new Date())
					.gtiCreatedBy("luizEdu").gtiModifiedAt(new Date()).gtiModifiedBy("luizEdu").gtiVersion(1).buld();
			contatoService.cadastrar(contato);
			ContatoAssert.assertThat(contatoService.findById(1L)).hasNome("Luiz Eduardo Silva Andrade");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
