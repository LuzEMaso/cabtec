package br.com.proverh.prova.contato;

import static org.junit.Assert.*;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.proverh.prova.exception.DAOException;
import br.com.proverh.prova.exception.RegraDeNegocioException;
import br.com.proverh.prova.service.ContatoService;

@RunWith(Arquillian.class)
public class ListarCincoRecadosMaisAntigosTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(WebArchive.class, "test.war").addPackages(true, "org.assertj.core")
				.addPackages(true, "java.util.logging.Logger").addPackages(true, "org.apache.commons.lang3")
				.addPackages(true, "br.com.proverh.prova").addPackages(true, "io.jsonwebtoken")
				.addPackages(true, "com.fasterxml.jackson")
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml").addAsResource("import.sql")
				.addAsWebInfResource("test-ds.xml", "test	-ds.xml");
	}

	@Inject
	private ContatoService contatoService;

	@Test
	@InSequence(value = 1)
	public void whenPossui_5ContatosAntigos_shouldRetornar_5Registros() throws DAOException, RegraDeNegocioException {
		assertEquals(contatoService.listarCincoRecadosMaiAntigos().size(), 5);
	}

}
