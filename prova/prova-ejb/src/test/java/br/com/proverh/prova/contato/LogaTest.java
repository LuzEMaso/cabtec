package br.com.proverh.prova.contato;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.proverh.prova.asserts.ContatoAssert;
import br.com.proverh.prova.build.LoginTOBuilder;
import br.com.proverh.prova.model.enuns.Mensagem;
import br.com.proverh.prova.model.to.LoginTO;
import br.com.proverh.prova.service.ContatoService;

@RunWith(Arquillian.class)
public class LogaTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(WebArchive.class, "test.war").addPackages(true, "org.assertj.core")
				.addPackages(true, "org.apache.commons.lang3").addPackages(true, "br.com.proverh.prova")
				.addPackages(true, "io.jsonwebtoken").addPackages(true, "com.fasterxml.jackson")
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml").addAsResource("import.sql")
				.addAsWebInfResource("test-ds.xml", "test-ds.xml");
	}

	@Inject
	private ContatoService contatoService;

	@Test
	@InSequence(value = 1)
	public void whenLoginVazio_shouldRetornarLoginNaoPodeSerVazio() {
		LoginTO loginTO = new LoginTOBuilder().login("").senha("luiz123").build();
		try {
			contatoService.logar(loginTO);
			fail("Expected an RegraDeNegocioException to be thrown");
		} catch (Exception e) {
			assertEquals(Mensagem.LOGIN_VAZIO.getValue(), e.getMessage());
		}
	}

	@Test
	@InSequence(value = 2)
	public void whenSenhaVazio_shouldRetornarSenhaNaoPodeSerVazio() {
		LoginTO loginTO = new LoginTOBuilder().login("luizMaso").senha("").build();
		try {
			contatoService.logar(loginTO);
			fail("Expected an RegraDeNegocioException to be thrown");
		} catch (Exception e) {
			assertEquals(Mensagem.SENHA_VAZIO.getValue(), e.getMessage());
		}
	}

	@Test
	@InSequence(value = 3)
	public void whenLoginNaoCadastrado_shouldRetornarLoginNaoCadastrado() {
		LoginTO loginTO = new LoginTOBuilder().login("luizMaso").senha("luiz123").build();
		try {
			contatoService.logar(loginTO);
			fail("Expected an RegraDeNegocioException to be thrown");
		} catch (Exception e) {
			assertEquals(Mensagem.CONTATO_NAO_CADASTRADO.getValue(), e.getMessage());
		}
	}

	@Test
	@InSequence(value = 4)
	public void whenSenhaInvalida_shouldRetornarLoginOuSenhaInvalida() {
		LoginTO loginTO = new LoginTOBuilder().login("LuizEdu").senha("luiz1234").build();
		try {
			contatoService.logar(loginTO);
			fail("Expected an RegraDeNegocioException to be thrown");
		} catch (Exception e) {
			assertEquals(Mensagem.ERRO_LOGIN.getValue(), e.getMessage());
		}
	}

	@Test
	@InSequence(value = 5)
	public void whenEfetuadoComSucesso_shouldRetornarDadosUsuario() {
		LoginTO loginTO = new LoginTOBuilder().login("LuizEdu").senha("luiz123").build();
		try {
			ContatoAssert.assertThat(contatoService.logar(loginTO)).hasNome("Luiz Eduardo");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
