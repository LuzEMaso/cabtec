package br.com.proverh.prova.contato;

import static org.junit.Assert.*;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.proverh.prova.asserts.ContatoAssert;
import br.com.proverh.prova.build.ContatoParamBuilder;
import br.com.proverh.prova.param.ContatoParam;
import br.com.proverh.prova.service.ContatoService;

@RunWith(Arquillian.class)
public class PesquisaTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(WebArchive.class, "test.war").addPackages(true, "org.assertj.core")
				.addPackages(true, "org.apache.commons.lang3").addPackages(true, "br.com.proverh.prova")
				.addPackages(true, "io.jsonwebtoken").addPackages(true, "com.fasterxml.jackson")
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml").addAsResource("import.sql")
				.addAsWebInfResource("test-ds.xml", "test-ds.xml");
	}

	@Inject
	private ContatoService contatoService;

	@Test
	@InSequence(value = 1)
	public void whenPesquisarPorEmail_shouldRetornarTamanho_1() {
		try {
			ContatoParam contatoParam = new ContatoParamBuilder().login("").nome("").ramal(null)
					.email("luizcmaso@gmail.com").build();
			Assert.assertEquals(contatoService.pesquisar(contatoParam).size(), 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@InSequence(value = 2)
	public void whenPesquisarPorEmail_shouldRetornarNomeContato() {
		try {
			ContatoParam contatoParam = new ContatoParamBuilder().login("").nome("").ramal(null)
					.email("luizcmaso@gmail.com").build();
			ContatoAssert.assertThat(contatoService.pesquisar(contatoParam).get(0)).hasNome("Luiz Eduardo");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@InSequence(value = 3)
	public void whenPesquisarPorLogin_shouldRetornarNomeContato() {
		try {
			ContatoParam contatoParam = new ContatoParamBuilder().login("LuizEdu").nome("").ramal(null).email("")
					.build();
			ContatoAssert.assertThat(contatoService.pesquisar(contatoParam).get(0)).hasNome("Luiz Eduardo");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@InSequence(value = 4)
	public void whenPesquisarPorMome_shouldRetornarTamanho_1() {
		try {
			ContatoParam contatoParam = new ContatoParamBuilder().login("").nome("Luiz Eduardo").ramal(null).email("")
					.build();
			assertEquals(contatoService.pesquisar(contatoParam).size(), 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@InSequence(value = 5)
	public void whenPesquisarPorNomeERamal_shouldRetornarNomeContato() {
		try {
			ContatoParam contatoParam = new ContatoParamBuilder().login("").nome("Luiz Eduardo").ramal(1234).email("")
					.build();
			ContatoAssert.assertThat(contatoService.pesquisar(contatoParam).get(0)).hasNome("Luiz Eduardo");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
