package br.com.proverh.prova.recado;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.proverh.prova.asserts.RecadoAssert;
import br.com.proverh.prova.model.entity.Recado;
import br.com.proverh.prova.model.enuns.Mensagem;
import br.com.proverh.prova.service.RecadoService;

@RunWith(Arquillian.class)
public class MarcaComoNaoLidoTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(WebArchive.class, "test.war").addPackages(true, "org.assertj.core")
				.addPackages(true, "org.apache.commons.lang3").addPackages(true, "br.com.proverh.prova")
				.addPackages(true, "io.jsonwebtoken").addPackages(true, "com.fasterxml.jackson")
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml").addAsResource("import.sql")
				.addAsWebInfResource("test-ds.xml", "test-ds.xml");
	}

	@Inject
	private RecadoService recadoService;

	@Test
	@InSequence(value = 1)
	public void whenRecadoEstaVazio_shouldRetornarRecadoNaoPodeSerVazio() {
		try {
			Recado recado = recadoService.findById(3L);
			recadoService.marcaComoNaoLido(recado);
			fail("Expected an RegraDeNegocioException to be thrown");
		} catch (Exception e) {
			assertEquals(Mensagem.RECADO_VAZIO.getValue(), e.getMessage());
		}
	}

	@Test
	@InSequence(value = 2)
	public void whenRecadoNaoFoiLido_shouldRetornarRecadoJaNaoLido() {
		try {
			Recado recado = recadoService.findById(2L);
			recadoService.marcaComoNaoLido(recado);
			fail("Expected an RegraDeNegocioException to be thrown");
		} catch (Exception e) {
			assertEquals(Mensagem.RECADO_NAO_LIDO.getValue(), e.getMessage());
		}
	}

	@Test
	@InSequence(value = 3)
	public void whenFeitoComSucesso_shouldRetornarRecadoLido() {
		try {
			Recado recado = recadoService.findById(1L);
			recadoService.marcaComoNaoLido(recado);
			RecadoAssert.assertThat(recadoService.findById(1L)).hasIndLido(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
