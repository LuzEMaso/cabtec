package br.com.proverh.prova.recado;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Date;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.proverh.prova.asserts.RecadoAssert;
import br.com.proverh.prova.build.RecadoBuilder;
import br.com.proverh.prova.model.entity.Contato;
import br.com.proverh.prova.model.entity.Recado;
import br.com.proverh.prova.model.enuns.Mensagem;
import br.com.proverh.prova.service.ContatoService;
import br.com.proverh.prova.service.RecadoService;

@RunWith(Arquillian.class)
public class RegistraTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(WebArchive.class, "test.war").addPackages(true, "org.assertj.core")
				.addPackages(true, "org.apache.commons.lang3").addPackages(true, "br.com.proverh.prova")
				.addPackages(true, "io.jsonwebtoken").addPackages(true, "com.fasterxml.jackson")
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml").addAsResource("import.sql")
				.addAsWebInfResource("test-ds.xml", "test-ds.xml");
	}

	@Inject
	private RecadoService recadoService;

	@Inject
	private ContatoService contatoService;

	@Test
	@InSequence(value = 1)
	public void whenContatoEstaVazio_shouldRetornarContatoNaoPodeSerVazio() {
		try {	
			Recado recado = new RecadoBuilder().idContato(null).horario(new Date()).quem("").texto("").indLido(1)
					.gtiCreatedAt(new Date()).gtiCreatedBy("luizEdu").gtiModifiedAt(new Date())
					.gtiModifiedByd("luizEdu").gtiVersion(1).buld();
			recadoService.registrar(recado);
			fail("Expected an RegraDeNegocioException to be thrown");
		} catch (Exception e) {
			assertEquals(Mensagem.CONTATO_VAZIO.getValue(), e.getMessage());
		}
	}

	@Test
	@InSequence(value = 2)
	public void whenDadosCompletos_shouldRetornarQuemCriou() {
		try {
			Contato contato = contatoService.findById(2L);
			Recado recado = new RecadoBuilder().idContato(contato).horario(new Date()).quem("Luiz").texto("Olá ")
					.indLido(1).gtiCreatedAt(new Date()).gtiCreatedBy("luizEdu").gtiModifiedAt(new Date())
					.gtiModifiedByd("luizEdu").gtiVersion(1).buld();
			recadoService.registrar(recado);
			RecadoAssert.assertThat(recadoService.findById(1L)).hasQuem("Luiz");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
