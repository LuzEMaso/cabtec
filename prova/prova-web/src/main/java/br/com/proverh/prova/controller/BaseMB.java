package br.com.proverh.prova.controller;

import javax.inject.Inject;

import br.com.proverh.prova.util.FacesUtil;

public class BaseMB {
	@Inject
	private FacesUtil facesUtil = new FacesUtil();

	protected FacesUtil getFacesUtil() {
		return facesUtil;
	}
}
