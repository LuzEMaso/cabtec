package br.com.proverh.prova.controller;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.proverh.prova.exception.DAOException;
import br.com.proverh.prova.model.entity.Contato;
import br.com.proverh.prova.service.ContatoService;
import lombok.Getter;
import lombok.Setter;

@Named
@ViewScoped
public class ContatoMB extends BaseMB {

	@Getter
	@Setter
	private Contato contato;

	@Inject
	private ContatoService contatoService;

	@PostConstruct
	public void init() {
		contato = new Contato();
	}

	public void salvar() {
		try {
			contatoService.cadastrar(contato);
			getFacesUtil().exibirMensagemInfo(null, "MSG_SUCESSO");
			contato = new Contato();
		} catch (DAOException e) {
			e.printStackTrace();
			getFacesUtil().exibirMensagemErro(null, e.getMessage());
		}
	}

}
