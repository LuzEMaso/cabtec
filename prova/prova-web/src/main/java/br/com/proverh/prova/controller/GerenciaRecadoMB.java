package br.com.proverh.prova.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import br.com.proverh.prova.exception.RegraDeNegocioException;
import br.com.proverh.prova.model.entity.Contato;
import br.com.proverh.prova.model.entity.Recado;
import br.com.proverh.prova.service.ContatoService;
import br.com.proverh.prova.service.RecadoService;
import lombok.Getter;
import lombok.Setter;

@Named
@ViewScoped
public class GerenciaRecadoMB extends BaseMB {

	@Getter
	@Setter
	private List<Recado> meusRecados;

	@Getter
	@Setter
	private Recado recadoSelected;

	@Getter
	@Setter
	private Boolean foiLidoBoolean;

	@Getter
	@Setter
	private Recado recado;

	@Inject
	private ContatoService contatoService;

	@Inject
	private RecadoService recadoService;

	@Getter
	@Setter
	private Contato contato;

	@PostConstruct
	public void init() {
		recado = new Recado();
		contato = receberContato();
		meusRecados = new ArrayList<>();
		meusRecados.addAll(listarMeusRecados());
	}

	private Collection<Recado> listarMeusRecados() {
		try {
			return recadoService.listarTodosRecadosContato(contato.getId());
		} catch (Exception e) {
			getFacesUtil().exibirMensagemErro(null, e.getMessage());
			return new ArrayList<>();
		}

	}

	private Contato receberContato() {
		try {
			// return (Contato)
			// FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("contato");
			return contatoService.findById(2L);
		} catch (Exception e) {
			return new Contato();
		}
	}

	public void abrirDetalhe() {
		try {
			recadoService.marcaComoLido(recadoSelected);
			foiLidoBoolean = recadoSelected.getIndLido() == 0 ? false : true;
			recado = recadoSelected;
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('dlgDetalheRecado').show();");
		} catch (RegraDeNegocioException r) {
			recado = recadoSelected;
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('dlgDetalheRecado').show();");
		} catch (Exception e) {
			getFacesUtil().exibirMensagemErro(null, e.getMessage());
		}
	}

	public void marcarComoNaoLido() {
		try {
			recadoService.marcaComoNaoLido(recado);
		} catch (Exception e) {
			getFacesUtil().exibirMensagemErro(null, e.getMessage());
		}

	}

	public String foiLido(Recado recado) {
		try {
			return recado.getIndLido() == 0 ? "Não" : "Sim";
		} catch (Exception e) {
			return "";
		}
	}

}
