package br.com.proverh.prova.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.proverh.prova.exception.DAOException;
import br.com.proverh.prova.exception.RegraDeNegocioException;
import br.com.proverh.prova.model.entity.Contato;
import br.com.proverh.prova.model.entity.Recado;
import br.com.proverh.prova.service.RecadoService;
import lombok.Getter;
import lombok.Setter;

@Named
@ViewScoped
public class InicioMB extends BaseMB {

	@Getter
	@Setter
	private Contato contato;

	@Getter
	@Setter
	private List<Recado> recados;

	@Setter
	private String saudacao;

	@Inject
	private RecadoService recadoService;

	@PostConstruct
	public void init() {
		recados = new ArrayList<>();
		contato = receberContato();
	}

	private Contato receberContato() {
		try {
			return (Contato) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("contato");
			// return contatoService.findById(2L);
		} catch (Exception e) {
			return new Contato();
		}
	}

	private void listarRecadosDoDia() {
		try {
			recados.clear();
			recados.addAll(recadoService.listarTodosDoDia(contato.getId()));
		} catch (DAOException e) {
			e.printStackTrace();
			getFacesUtil().exibirMensagemErro(null, e.getMessage());
		} catch (RegraDeNegocioException e) {
			e.printStackTrace();
			getFacesUtil().exibirMensagemErro(null, e.getMessage());
		}
	}

	private Integer retornarRecadosDoDia() {
		listarRecadosDoDia();
		return recados.size();
	}

	public String getSaudacao() {
		StringBuilder sb = new StringBuilder();
		return sb.append("Seja Bem Vindo! ").append(contato.getNome()).append(" você possui: ")
				.append(retornarRecadosDoDia()).append(" recados de hoje!").toString();
	}

}
