package br.com.proverh.prova.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import br.com.proverh.prova.model.entity.Contato;
import br.com.proverh.prova.model.enuns.Mensagem;
import br.com.proverh.prova.model.to.LoginTO;
import br.com.proverh.prova.service.ContatoService;
import lombok.Getter;
import lombok.Setter;

@Named
@SessionScoped
public class LoginMB extends BaseMB implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private Contato contato;

	@Getter
	@Setter
	private LoginTO loginTO;

	@Inject
	private ContatoService contatoService;

	@Getter
	@Setter
	private boolean loggedIn;

	@PostConstruct
	public void init() {
		contato = new Contato();
		loginTO = new LoginTO();
	}

	public String logar() {
		try {
			contato = contatoService.logar(loginTO);
			if (contato != null) {
				HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext()
						.getSession(true);
				session.setAttribute("contato", this.contato);
				loggedIn = true;
				return "/paginas/area/inicio.xhtml?faces-redirect=true";
			} else {
				loggedIn = false;
				getFacesUtil().exibirMensagemErro(null, Mensagem.ERRO_LOGIN.getValue());
				return "";
			}
		} catch (Exception e) {
			getFacesUtil().exibirMensagemErro(null, Mensagem.ERRO_LOGIN.getValue());
			return "";
		}

	}

}
