package br.com.proverh.prova.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import br.com.proverh.prova.exception.DAOException;
import br.com.proverh.prova.model.entity.Contato;
import br.com.proverh.prova.model.entity.Recado;
import br.com.proverh.prova.param.ContatoParam;
import br.com.proverh.prova.service.ContatoService;
import br.com.proverh.prova.service.RecadoService;
import lombok.Getter;
import lombok.Setter;

@Named
@ViewScoped
public class RecadoMB extends BaseMB {

	@Getter
	@Setter
	private Recado recado;

	@Getter
	@Setter
	private List<Contato> contatos;

	@Getter
	@Setter
	private Contato contato;

	@Getter
	@Setter
	private Contato contatoSelected;

	@Getter
	@Setter
	private ContatoParam contatoParam;

	@Inject
	private ContatoService contatoService;

	@Inject
	private RecadoService recadoService;

	@PostConstruct
	public void init() {
		recado = new Recado();
		contato = receberContato();
		contatos = new ArrayList<>();
		contatoParam = new ContatoParam();
	}

	public void registrar() {
		try {
			recadoService.registrar(recado);
			getFacesUtil().exibirMensagemInfo(null, "MSG_SUCESSO_RECADO");
			recado = new Recado();
		} catch (DAOException e) {
			e.printStackTrace();
			getFacesUtil().exibirMensagemErro(null, e.getMessage());
		}
	}

	public void pesquisarContato() {
		try {
			contatos.clear();
			contatos = contatoService.pesquisar(contatoParam);
			contatoParam = new ContatoParam();
		} catch (Exception e) {
			e.printStackTrace();
			getFacesUtil().exibirMensagemErro(null, e.getMessage());
		}
	}

	public void adicionarContato() {
		recado.setIdContato(contatoSelected);
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('dlgPesquisaContato').hide();");

	}

	private Contato receberContato() {
		try {
			return (Contato) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("contato");
		} catch (Exception e) {
			return new Contato();
		}
	}

}
