package br.com.proverh.prova.filter;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.proverh.prova.controller.LoginMB;

@WebFilter("/paginas/*")
public class LoginFilter implements Filter {

	@Inject
	private LoginMB loginMB;

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest parRequest, ServletResponse parResponse, FilterChain chain)
			throws IOException, ServletException {
		if (loginMB == null || !loginMB.isLoggedIn()) {
			String contextPath = ((HttpServletRequest) parRequest).getContextPath();
			((HttpServletResponse) parResponse).sendRedirect(contextPath + "/login.xhtml");
		} else {
			chain.doFilter(parRequest, parResponse);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

}
