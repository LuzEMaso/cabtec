package br.com.proverh.prova.util;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.ResourceBundle;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

@Stateless
@LocalBean
public class FacesUtil implements Serializable {

	private static final long serialVersionUID = 6976146781073140205L;
	private ResourceBundle arquivoDeMensagens = new ResourceBundleUTF8();

	public void exibirMensagemInfo(String idComponente, String chaveArquivoBundle) {
		FacesContext.getCurrentInstance().addMessage(idComponente,
				criarMensagem(FacesMessage.SEVERITY_INFO, chaveArquivoBundle));
	}

	public void exibirMensagemAlerta(String idComponente, String chaveArquivoBundle) {
		FacesContext.getCurrentInstance().addMessage(idComponente,
				criarMensagem(FacesMessage.SEVERITY_WARN, chaveArquivoBundle));
	}

	public void exibirMensagemErro(String idComponente, String chaveArquivoBundle) {
		FacesContext.getCurrentInstance().addMessage(idComponente,
				criarMensagem(FacesMessage.SEVERITY_ERROR, chaveArquivoBundle));
	}

	public void exibirMensagemErroFatal(String idComponente, String chaveArquivoBundle) {
		FacesContext.getCurrentInstance().addMessage(idComponente,
				criarMensagem(FacesMessage.SEVERITY_FATAL, chaveArquivoBundle));
	}

	public void exibirMensagensComQuebraDeLinha(String strTodasMensagens, FacesMessage.Severity severity) {
		if (StringUtils.isNotBlank(strTodasMensagens)) {
			String[] mensagens = strTodasMensagens.split("\n");
			for (String msg : mensagens) {
				if (FacesMessage.SEVERITY_INFO == severity) {
					exibirMensagemInfo(null, msg);
				} else if (FacesMessage.SEVERITY_WARN == severity) {
					exibirMensagemAlerta(null, msg);
				} else if (FacesMessage.SEVERITY_ERROR == severity) {
					exibirMensagemErro(null, msg);
				} else if (FacesMessage.SEVERITY_FATAL == severity) {
					exibirMensagemErroFatal(null, msg);
				}
				if (severity == null) {
					exibirMensagemErro(null, msg);
				}
			}
		}
	}

	public void redirecionar(String urlDestino) throws IOException {
		getExternalContext().redirect(getRootWebApp().concat(urlDestino));
	}

	public boolean isPostBack() {
		return FacesContext.getCurrentInstance().isPostback();
	}

	public ExternalContext getExternalContext() {
		return FacesContext.getCurrentInstance().getExternalContext();
	}

	public String getRootWebApp() {
		return FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
	}

	public String getRequestParameter(String nomeParametro) {
		return (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get(nomeParametro);
	}

	public String getInitParameter(String nomeParametro) {
		return FacesContext.getCurrentInstance().getExternalContext().getInitParameter(nomeParametro);
	}

	public Map<String, Object> getSessionMap() {
		return FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	}

	public ServletContext getServletContext() {
		return (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	}

	public HttpServletRequest getServletRequest() {
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}

	public HttpServletResponse getServletResponse() {
		return (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
	}

	private FacesMessage criarMensagem(FacesMessage.Severity severity, String chaveArquivoBundle) {
		if (chaveArquivoBundle != null)
			if (arquivoDeMensagens.containsKey(chaveArquivoBundle))
				return new FacesMessage(severity, "", arquivoDeMensagens.getString(chaveArquivoBundle));
			else
				return new FacesMessage(severity, "", chaveArquivoBundle);
		else
			return new FacesMessage(severity, "", StringUtils.EMPTY);
	}
}