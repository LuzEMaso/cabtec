package br.com.proverh.prova.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;

public class ResourceBundleUTF8 extends ResourceBundle {

	protected static final String BUNDLE_NAME = "messages";
	protected static final String BUNDLE_EXTENSION = "properties";
	protected static final String CHARSET = "UTF-8";
	protected static final ResourceBundle.Control UTF8_CONTROL = new UTF8Control();

	public ResourceBundleUTF8() {
		try {
			setParent(ResourceBundle.getBundle(BUNDLE_NAME, FacesContext.getCurrentInstance().getViewRoot().getLocale(),
					UTF8_CONTROL));
		} catch (Exception e) {
		}

	}

	protected Object handleGetObject(String key) {
		return this.parent.getObject(key);
	}

	public Enumeration<String> getKeys() {
		return this.parent.getKeys();
	}

	protected static class UTF8Control extends ResourceBundle.Control {

		public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader,
				boolean reload) throws IllegalAccessException, InstantiationException, IOException {

			String bundleName = toBundleName(baseName, locale);
			String resourceName = toResourceName(bundleName, "properties");
			ResourceBundle bundle = null;
			InputStream stream = null;

			if (reload) {
				URL url = loader.getResource(resourceName);
				if (url != null) {
					URLConnection connection = url.openConnection();
					if (connection != null) {
						connection.setUseCaches(false);
						stream = connection.getInputStream();
					}
				}
			} else {
				stream = loader.getResourceAsStream(resourceName);
			}

			if (stream != null) {
				try {
					bundle = new PropertyResourceBundle(new InputStreamReader(stream, "UTF-8"));
				} finally {
					stream.close();
				}
			}

			return bundle;
		}
	}
}